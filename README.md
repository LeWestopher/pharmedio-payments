## Pharmedio Payments Gateway Git Repository ##

This repository was created to house the development model of the Pharmedio Payments Gateway.  The gateway is designed to communicate with the Balanced Payments API (http://www.balancedpayments.com) in a RESTful manner to coordinate ACH debit and credits against customer's bank accounts.  

### What has been completed? ###

* A basic DB schema for testing has been created
* AJAX Endpoints for Add Account and Delete Account complete
* A Test API Key has been generated for project use:
* The Balanced, HTTPful, and RESTful libraries have been loaded into CakePHP for use.

### What needs to be done? ###

* Balanced.js needs to be set up and implemented for compliant bank account additions
* <strike>Encrypt function for Account/Routing numbers</strike> ** REMOVED **
* <strike>Decrypt function for Account/Routing numbers</strike> ** REMOVED **
* Customer information needs to be stored in our DB schema as a token, not as account/routing numbers.  An AJAX endpoint needs to be written for this.
* AJAX endpoint to charge a customer's token (public function charge())
* Full DB implementation with the binary tree behavior
* Needs to be front-end prettified
* MORE CAFFEINE.

