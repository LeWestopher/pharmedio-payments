<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<table>
	<tr>
		<th>Name</th>
		<th>Account Number</th>
		<th>Routing Number</th>
		<th>Account Type</th>
	</tr>
	<?php foreach ($customers as $customer): ?>
	<tr>
		<td><?php echo $customer['Customer']['name']; ?></td>
		<td><?php echo $customer['Customer']['account']; ?></td>
		<td><?php echo $customer['Customer']['routing']; ?></td>
		<td><?php echo $customer['Customer']['type']; ?>
	</tr>
	<?php endforeach; ?>
</table>