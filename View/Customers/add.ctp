<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<div id="contentSecond"><?php echo $this->session->Flash(); ?></div>
<table>
	<tr>
		<th>Name</th>
		<th>Account Number</th>
		<th>Routing Number</th>
		<th>Account Type</th>
		<th>Remove</th>
	</tr>
	<?php foreach ($elements as $element): ?>
	<tr>
		<td><?php echo $element['Customer']['name']; ?></td>
		<td><?php echo $element['Customer']['account']; ?></td>
		<td><?php echo $element['Customer']['routing']; ?></td>
		<td><?php echo $element['Customer']['type']; ?></td>
		<td><p class="<?php echo $element['Customer']['id']; ?>">Delete</p></td>
	</tr>
		<?php
			$value = '{id:' . $element['Customer']['id'] . '}';
			$this->Js->get('p.' . $element['Customer']['id'])->event(
				'click',
				$this->Js->request(
					array(
						'action' => 'remove'
					),
					array(
						'update' => 'div.contentTable', 
						'data' => $value,
						'async' => true, 
						'dataExpression' => true, 
						'method' => 'POST'
					)
				)
			);
		?>

	<?php endforeach; ?>
</table>
<?php echo $this->paginator->prev(); ?> – &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo $this->paginator->numbers(array('separator'=>' – ')); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo $this->paginator->next('Next Page'); ?>
<?php echo $this->Js->writeBuffer(); ?>