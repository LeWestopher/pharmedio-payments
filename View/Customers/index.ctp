<div id="contentSecond"><?php echo $this->session->Flash(); ?></div>
<div class="contentTable">
	<table>
		<tr>
			<th>Name</th>
			<th>Account Number</th>
			<th>Routing Number</th>
			<th>Account Type</th>
			<th>Charge</th>
			<th>Remove</th>
		</tr>
		<?php foreach ($customers as $customer): ?>
		<tr>
			<td><?php echo $customer['Customer']['name']; ?></td>
			<td><?php echo $customer['Customer']['account']; ?></td>
			<td><?php echo $customer['Customer']['routing']; ?></td>
			<td><?php echo $customer['Customer']['type']; ?></td>
			<td><?php 
				echo $this->Form->create('customer' . $customer['Customer']['id']);
				echo $this->Form->input('amount');
				echo $this->Form->input('id', array('value' => $customer['Customer']['id'], 'type' => 'hidden', 'div' => false));
				echo $this->Form->end('Submit', array('div' => false));
			?>
			<td><p class="<?php echo $customer['Customer']['id']; ?>">Delete</p></td>
		</tr>
		<?php
			$value = '{id:' . $customer['Customer']['id'] . '}';
			$this->Js->get('p.' . $customer['Customer']['id'])->event(
				'click',
				$this->Js->request(
					array(
						'action' => 'remove'
					),
					array(
						'update' => 'div.contentTable', 
						'data' => $value,
						'async' => true, 
						'dataExpression' => true, 
						'method' => 'POST'
					)
				)
			);
		?>
		<?php endforeach; ?>
	</table>
<?php $this->Paginator->options(array('update' => 'div.contentTable', 'evalScripts' => true));?>
<?php echo $this->paginator->prev(); ?> – &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo $this->paginator->numbers(array('separator'=>' – ')); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo $this->paginator->next('Next Page'); ?>
</div>
<div id="accountAdd">
	<?php
	//	$data = $this->Js->get('#CustomerAddForm')->serializeForm(array('isForm' => true, 'inline' => true));
	//	$this->Js->get('#CustomerAddForm')->event(
	//		'submit',
	//		$this->Js->request(
	//			array('action' => 'add'),
	//			array('update' => '#contentTable', 'data' => $data, 'async' => true, 'dataExpression' => true, 'method' => 'POST')
	//			)
	//		);
		echo $this->form->Create('Customer', array('action' => 'add', 'default' => false));
		echo $this->form->input('name');
		echo $this->form->input('account');
		echo $this->form->input('routing');
		echo $this->form->input('type');
		
		$options = array('div' => array('id' => 'submit'));
		
		echo $this->form->end('submit', $options);
		echo $this->Js->writeBuffer();
	?>
</div>
<script>
$(document).ready(function () {
	$("#CustomerAddForm").bind("submit", function (event) {
		$.ajax({
			async:true, 
			data:$("#CustomerAddForm").serialize(), 
			dataType:"html", 
			success:function (data, textStatus) {
				$("div.contentTable").html(data);
				}, 
			type:"POST", 
			url:"\/customers\/add"});
		return false;
	});
});
</script>