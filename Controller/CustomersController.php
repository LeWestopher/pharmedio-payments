<?php

class CustomersController extends AppController {
	public $helpers = array('Html', 'Form', 'Js', 'Paginator');
	public $components = array('RequestHandler');
	
	public $paginate = array(
		'limit' => 5,
		'order' => array(
			'Customer.name' => 'asc'
			)
		);
	
	public function index() {
		// view for the main page
		$data = $this->paginate('Customer');
		$this->set('customers', $data);
	}
	
	public function add() {
		// AJAX endpoint to add new customers to the DB
		if ($this->request->is('ajax')) {
			$this->Customer->save($this->request->data);
			$this->set('elements', $this->paginate('Customer'));
			$this->render('add', 'ajax'); 
		}
	}
	
	public function charge($id) {
		// function that retrieves the SQL query for the chosen customer, inputs their information as a Balanced customer, and
		// charges that Balance customer via the Balanced Payments API
	}
	
	public function createCustomer() {
		// generates a new customer token with the Balanced API
		$newcustomer = new \Balanced\Customer(
			array(
				"name" => "William Henry Cavendish III",
				"email" => "william@example.com"
			)
		);
		$newcustomer->save();
		$data = $newcustomer->uri();
		$this->set('entry', $data);
	}

	
	public function verify($id) {
		// function that takes the user through the verify process to enable ACH transactions
	}
	
	public function remove() {
		// function that removes the customer from our SQL database
		if ($this->request->is('ajax')) {
			$this->Customer->delete($this->request->data('id'));
			$this->Session->setFlash('The Customer has been deleted.');
			$this->set('elements', $this->paginate('Customer'));
			$this->render('add', 'ajax');
		}
	}
	
	public function edit($id) {
		// function that allows editing of a customer on our SQL database
	}
	
	private function encrypt() {
		// function that encrypts the customer's financial information as a salted hash to the DB
		// DO NOT USE DO NOT USE DO NOT USE DO NOT USE DO NOT USE DO NOT USE DO NOT USE DO NOT USE
	}
	
	private function decrypt() {
		// function that decrypts the salted hash stored in the DB to financial info for use with the Balanced API
		// DO NOT USE DO NOT USE DO NOT USE DO NOT USE DO NOT USE DO NOT USE DO NOT USE DO NOT USE DO NOT USE DO NOT USE
	}
}
	
	